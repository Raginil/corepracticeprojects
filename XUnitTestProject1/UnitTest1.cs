using CoreWebAPI.Controllers;
using CoreWebAPI.Repositories;
using EFCoreDBFirst;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using Xunit;


namespace XUnitTestProject1
{
    public class UnitTest1
    {
        private readonly EFCoreDBFContext db;
        [Fact]
        public void Test1()
        {
            var Mockrepo = new Mock<IEmployeeRepository>();
            Mockrepo.Setup(x => x.GetAllEmployees())
        .Returns(new List<Employee>());

            var controller = new EmployeeController(Mockrepo.Object);

            // Act
            ActionResult<IEnumerable<Employee>> actionResult = controller.Get();
            var contentResult = actionResult.Result;

            // Assert
            Assert.NotNull(contentResult);

            Assert.IsType<ActionResult<IEnumerable<Employee>>>(actionResult);
                //.IsType(typeof(ActionResult<Employee>));
        }

        [Fact]
        public void Test2()
        {
            var Mockrepo = new Mock<IEmployeeRepository>();
            Mockrepo.Setup(x => x.GetById(1))
        .Returns(new Employee { Id = 1 });

            var controller = new EmployeeController(Mockrepo.Object);

            // Act
            ActionResult<Employee> actionResult = controller.Get(1);
            var contentResult = actionResult.Result;

            // Assert
            Assert.NotNull(contentResult);

            Assert.IsType<ActionResult<Employee>>(actionResult);
            
        }
    }
}
