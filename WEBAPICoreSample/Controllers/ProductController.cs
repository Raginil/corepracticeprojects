﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EFCoreCFTest;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace WEBAPICoreSample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository _Repo { get; set; }
        private EFCoreCodeFirstTestContext _db { get; set; }
        public ProductController(IProductRepository Repository, EFCoreCodeFirstTestContext Context)
        {
            _Repo = Repository;
            _db = Context;
        }


        /// <summary>
        /// Get all Products
        /// </summary>
        /// <returns></returns>
        
        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            var Products = _Repo.GetAllProducts();
            if (Products != null)
            {
                return Ok(Products);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("serachProducts")]
        public ActionResult<IEnumerable<Product>> SearchProducts(int id, string name, string desc)
        {
            var Products = _Repo.GetAllProducts().Where(x =>
            x.Id == (id > 0 ? id : x.Id)
            && x.Name == (name.Length > 0 ? name : x.Name)
            && x.Description == (desc.Length > 0 ? desc : x.Description)).ToList();
            if (Products != null)
            {
                return Ok(Products);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("serachProductsByHeader")]
        public ActionResult<IEnumerable<Product>> SearchProductsH([FromHeader]int id, [FromHeader]string name, [FromHeader] string desc)
        {
            var Products = _Repo.GetAllProducts().Where(x =>
            x.Id == (id > 0 ? id : x.Id)
            && x.Name == (name.Length > 0 ? name : x.Name)
           // && x.Description == (desc.Length > 0 ? desc : x.Description)
            ).ToList();
            if (Products != null)
            {
                return Ok(Products);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("serachProductsByRoute")]
        public ActionResult<IEnumerable<Product>> SearchProductsR([FromRoute] int id, [FromRoute] string name, [FromRoute] string desc)
        {
            var Products = _Repo.GetAllProducts().Where(x =>
            x.Id == (id > 0 ? id : x.Id)
            && x.Name == (name.Length > 0 ? name : x.Name)
            && x.Description == (desc.Length > 0 ? desc : x.Description)).ToList();
            if (Products != null)
            {
                return Ok(Products);
            }
            else
            {
                return NoContent();
            }
        }

        [HttpGet]
        [Route("serachProductsByQuery")]
        public ActionResult<IEnumerable<Product>> SearchProductsQ([FromQuery] int id, [FromQuery] string name, [FromQuery] string desc)
        {
            var Products = _Repo.GetAllProducts().Where(x =>
            x.Id == (id > 0 ? id : x.Id)
            && x.Name == (name.Length > 0 ? name : x.Name)
            && x.Description == (desc.Length > 0 ? desc : x.Description)).ToList();
            if (Products != null)
            {
                return Ok(Products);
            }
            else
            {
                return NoContent();
            }
        }



        /// <summary>
        /// Get Product by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var Product = _Repo.GetById(id);
            if (Product != null)
            {
                return Ok(Product);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// Add new Product
        /// </summary>
        /// <param name="objProduct"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post([FromBody] Product objProduct)
        {
            if (ModelState.IsValid)
            {
                _Repo.AddProduct(objProduct);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="id"></param>
        /// <param name="objProduct"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Product objProduct)
        {
            if (ModelState.IsValid)
            {
                _Repo.UpdateProduct(objProduct);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Delete a product by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            Product pd = _Repo.GetById(id);
            if (pd != null)
            {
                _Repo.RemoveProduct(id);
                return Ok();
            }
            
            return NotFound();
        }
    }
}
