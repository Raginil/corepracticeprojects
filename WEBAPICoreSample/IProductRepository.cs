﻿using EFCoreCFTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBAPICoreSample
{
   public interface IProductRepository
    {
        public IList<Product> GetAllProducts();
        public Product GetById(int Id);
        public int AddProduct(Product objProduct);
        public int RemoveProduct(int id);
        public int UpdateProduct(Product ObjProduct);
    }
}
