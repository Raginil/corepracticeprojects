﻿using EFCoreCFTest;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBAPICoreSample
{
    public class ProductRepository : IProductRepository
    {
        private readonly EFCoreCodeFirstTestContext db;
        
        public ProductRepository(EFCoreCodeFirstTestContext _db)
        {
            db = _db;
        }
        public int AddProduct(Product objProduct)
        {
            db.Add(objProduct);
            db.SaveChanges();
            return objProduct.Id;
        }

        public IList<Product> GetAllProducts()
        {
           return db.Products.ToList();
        }

        public Product GetById(int Id)
        {
            return db.Products.Include(x=>x.Details).Where(x => x.Id.Equals(Id)).FirstOrDefault();
        }

        public int RemoveProduct(int id)
        {
            Product pd = db.Products.FirstOrDefault(x => x.Id.Equals(id));
            db.Products.Remove(pd);
            db.SaveChanges();
            return pd.Id;
        }

        public int UpdateProduct(Product ObjProduct)
        {
            Product pd = db.Products.FirstOrDefault(x => x.Id.Equals(ObjProduct.Id));
            if (pd != null)
            {
                pd.Name = ObjProduct.Name;
                pd.Description = ObjProduct.Description;
                db.Update(pd);
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        
    }
}
