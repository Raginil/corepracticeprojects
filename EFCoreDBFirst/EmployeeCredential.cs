﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EFCoreDBFirst
{
    public partial class EmployeeCredential
    {
        public int EmployeeId { get; set; }
        public int UserId { get; set; }
    }
}
