﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreCFTest
{
    public class EFCoreCodeFirstTestContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<Category> Categorys { get; set; }

        public EFCoreCodeFirstTestContext(DbContextOptions<EFCoreCodeFirstTestContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning
//                //To protect potentially sensitive information in your connection string, 
//                //you should move it out of source code. You can avoid scaffolding the connection string 
//                //  by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Data Source=DELL\\SQLEXPRESS;Initial Catalog=EFCoreCodeFirstTest;Integrated Security=True");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category()
                {
                    CategoryId = 1,
                    CategoryName = "Stationary"

                });

            modelBuilder.Entity<Product>().HasData(
                new Product()
                {
                    Id = 1,
                    Name = "book",
                    Description = "Sample Book"
                },
                 new Product()
                 {
                     Id = 3,
                     Name = "books",
                     Description = "Sample Book3"
                 }

                );

            modelBuilder.Entity<ProductDetail>().HasData(
                new ProductDetail()
                {
                    Id = 1,
                    ProductId = 1,
                    Quantity = 4,
                    Price = 100
                });
        }
    }
}
