﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirstSamples.Migrations
{
    public partial class AddProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var procudure = @"Create Procedure sp_GetEmployeeByName(@name varchar(20))
as
begin
select EmployeeId,EmployeeName,Designation,Salary from dbo.Employee where EmployeeName like '%@name%'
end";
            migrationBuilder.Sql(procudure);
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
