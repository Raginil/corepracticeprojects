﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirstSamples.Migrations
{
    public partial class data_into_locationtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Location",
                columns: new[] { "LocationId", "LocationName" },
                values: new object[,]
                {
                    { 1, "Hyderabad" },
                    { 2, "Secunderabad" }
                });

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 1,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 44, 42, 828, DateTimeKind.Local).AddTicks(3463));

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 2,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 44, 42, 830, DateTimeKind.Local).AddTicks(8452));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Location",
                keyColumn: "LocationId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Location",
                keyColumn: "LocationId",
                keyValue: 2);

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 1,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 37, 36, 299, DateTimeKind.Local).AddTicks(5898));

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 2,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 37, 36, 302, DateTimeKind.Local).AddTicks(163));
        }
    }
}
