﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirstSamples.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "scl");

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    Designation = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    Salary = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationId);
                });

            migrationBuilder.CreateTable(
                name: "School",
                schema: "scl",
                columns: table => new
                {
                    SchoolId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SchoolName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    EstablishedDate = table.Column<DateTime>(type: "datetime2", unicode: false, nullable: false),
                    Specification = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    Location = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    SchoolType = table.Column<int>(type: "int", unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_School", x => x.SchoolId);
                });

            migrationBuilder.CreateTable(
                name: "Skill",
                columns: table => new
                {
                    SkillId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SkillName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    SkillType = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skill", x => x.SkillId);
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DepartmentName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    LocationId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.DepartmentId);
                    table.ForeignKey(
                        name: "FK_Department_Location",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeLocation",
                columns: table => new
                {
                    EmployeeLocationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(type: "int", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: false),
                    Locationype = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false, defaultValueSql: "('Home')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeLocation", x => x.EmployeeLocationId);
                    table.ForeignKey(
                        name: "FK_EmployeeLocation_Employee",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeLocation_Location",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeSkills",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(type: "int", nullable: false),
                    SkillId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeSkills", x => new { x.EmployeeId, x.SkillId });
                    table.ForeignKey(
                        name: "FK_EmployeeSkill_Employee",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeSkill_Skill",
                        column: x => x.SkillId,
                        principalTable: "Skill",
                        principalColumn: "SkillId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "scl",
                table: "School",
                columns: new[] { "SchoolId", "EstablishedDate", "Location", "SchoolName", "SchoolType", "Specification" },
                values: new object[] { 1, new DateTime(2020, 9, 2, 14, 37, 36, 299, DateTimeKind.Local).AddTicks(5898), "Hyderabad", "MppSchool", 1, "mandatoryEducation" });

            migrationBuilder.InsertData(
                schema: "scl",
                table: "School",
                columns: new[] { "SchoolId", "EstablishedDate", "Location", "SchoolName", "SchoolType", "Specification" },
                values: new object[] { 2, new DateTime(2020, 9, 2, 14, 37, 36, 302, DateTimeKind.Local).AddTicks(163), "Hyderabad", "Abcschool", 3, "StateSyllabus" });

            migrationBuilder.CreateIndex(
                name: "IX_Department_LocationId",
                table: "Department",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeLocation_EmployeeId",
                table: "EmployeeLocation",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeLocation_LocationId",
                table: "EmployeeLocation",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeSkills_SkillId",
                table: "EmployeeSkills",
                column: "SkillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Department");

            migrationBuilder.DropTable(
                name: "EmployeeLocation");

            migrationBuilder.DropTable(
                name: "EmployeeSkills");

            migrationBuilder.DropTable(
                name: "School",
                schema: "scl");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Skill");
        }
    }
}
