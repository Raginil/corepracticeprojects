﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirstSamples.Migrations
{
    public partial class SampledatatoAllTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Department",
                columns: new[] { "DepartmentId", "DepartmentName", "LocationId" },
                values: new object[,]
                {
                    { 1, "IT", null },
                    { 2, "Accounts", null },
                    { 3, "Learning", null }
                });

            migrationBuilder.InsertData(
                table: "Employee",
                columns: new[] { "EmployeeId", "Designation", "EmployeeName", "Salary" },
                values: new object[,]
                {
                    { 1, "SE", "Employee1", "20000" },
                    { 2, "SSE", "Employee2", "30000" },
                    { 3, "SE", "Employee3", "10000" }
                });

            migrationBuilder.InsertData(
                table: "Skill",
                columns: new[] { "SkillId", "SkillName", "SkillType" },
                values: new object[,]
                {
                    { 1, "Dotnet", "Software" },
                    { 2, "sjfhbashd", "design" },
                    { 3, "SqlServer", "Software" }
                });

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 1,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 17, 52, 32, 108, DateTimeKind.Local).AddTicks(6951));

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 2,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 17, 52, 32, 113, DateTimeKind.Local).AddTicks(1079));

            migrationBuilder.InsertData(
                table: "EmployeeLocation",
                columns: new[] { "EmployeeLocationId", "EmployeeId", "LocationId", "Locationype" },
                values: new object[,]
                {
                    { 1, 1, 1, "Home" },
                    { 3, 1, 1, "Office" },
                    { 2, 2, 2, "Office" },
                    { 4, 2, 2, "Home" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeSkills",
                columns: new[] { "EmployeeId", "SkillId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 1, 2 },
                    { 2, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Department",
                keyColumn: "DepartmentId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Department",
                keyColumn: "DepartmentId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Department",
                keyColumn: "DepartmentId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EmployeeLocation",
                keyColumn: "EmployeeLocationId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EmployeeLocation",
                keyColumn: "EmployeeLocationId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EmployeeLocation",
                keyColumn: "EmployeeLocationId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EmployeeLocation",
                keyColumn: "EmployeeLocationId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EmployeeSkills",
                keyColumns: new[] { "EmployeeId", "SkillId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeSkills",
                keyColumns: new[] { "EmployeeId", "SkillId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "EmployeeSkills",
                keyColumns: new[] { "EmployeeId", "SkillId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeSkills",
                keyColumns: new[] { "EmployeeId", "SkillId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "Skill",
                keyColumn: "SkillId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumn: "EmployeeId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Skill",
                keyColumn: "SkillId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Skill",
                keyColumn: "SkillId",
                keyValue: 2);

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 1,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 44, 42, 828, DateTimeKind.Local).AddTicks(3463));

            migrationBuilder.UpdateData(
                schema: "scl",
                table: "School",
                keyColumn: "SchoolId",
                keyValue: 2,
                column: "EstablishedDate",
                value: new DateTime(2020, 9, 2, 14, 44, 42, 830, DateTimeKind.Local).AddTicks(8452));
        }
    }
}
