﻿// <auto-generated />
using System;
using EFCodeFirstSamples.EFCoredbfirstModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EFCodeFirstSamples.Migrations
{
    [DbContext(typeof(EFDataFirstPOCContext))]
    [Migration("20200902090737_initialMigration")]
    partial class initialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0-preview.8.20407.4");

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.Department", b =>
                {
                    b.Property<int>("DepartmentId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("DepartmentName")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<int?>("LocationId")
                        .HasColumnType("int");

                    b.HasKey("DepartmentId");

                    b.HasIndex("LocationId");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.Employee", b =>
                {
                    b.Property<int>("EmployeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Designation")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<string>("EmployeeName")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Salary")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.HasKey("EmployeeId");

                    b.ToTable("Employee");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.EmployeeLocation", b =>
                {
                    b.Property<int>("EmployeeLocationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int");

                    b.Property<int>("LocationId")
                        .HasColumnType("int");

                    b.Property<string>("Locationype")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)")
                        .HasDefaultValueSql("('Home')");

                    b.HasKey("EmployeeLocationId");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("LocationId");

                    b.ToTable("EmployeeLocation");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.EmployeeSkills", b =>
                {
                    b.Property<int>("EmployeeId")
                        .HasColumnType("int");

                    b.Property<int>("SkillId")
                        .HasColumnType("int");

                    b.HasKey("EmployeeId", "SkillId");

                    b.HasIndex("SkillId");

                    b.ToTable("EmployeeSkills");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.Location", b =>
                {
                    b.Property<int>("LocationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("LocationName")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.HasKey("LocationId");

                    b.ToTable("Location");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.School", b =>
                {
                    b.Property<int>("SchoolId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<DateTime>("EstablishedDate")
                        .IsUnicode(false)
                        .HasColumnType("datetime2");

                    b.Property<string>("Location")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<string>("SchoolName")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<int>("SchoolType")
                        .IsUnicode(false)
                        .HasColumnType("int");

                    b.Property<string>("Specification")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.HasKey("SchoolId");

                    b.ToTable("School", "scl");

                    b.HasData(
                        new
                        {
                            SchoolId = 1,
                            EstablishedDate = new DateTime(2020, 9, 2, 14, 37, 36, 299, DateTimeKind.Local).AddTicks(5898),
                            Location = "Hyderabad",
                            SchoolName = "MppSchool",
                            SchoolType = 1,
                            Specification = "mandatoryEducation"
                        },
                        new
                        {
                            SchoolId = 2,
                            EstablishedDate = new DateTime(2020, 9, 2, 14, 37, 36, 302, DateTimeKind.Local).AddTicks(163),
                            Location = "Hyderabad",
                            SchoolName = "Abcschool",
                            SchoolType = 3,
                            Specification = "StateSyllabus"
                        });
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.Skill", b =>
                {
                    b.Property<int>("SkillId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("SkillName")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<string>("SkillType")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.HasKey("SkillId");

                    b.ToTable("Skill");
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.Department", b =>
                {
                    b.HasOne("EFCodeFirstSamples.EFCoredbfirstModels.Location", "Location")
                        .WithMany("Departments")
                        .HasForeignKey("LocationId")
                        .HasConstraintName("FK_Department_Location")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.EmployeeLocation", b =>
                {
                    b.HasOne("EFCodeFirstSamples.EFCoredbfirstModels.Employee", "Employee")
                        .WithMany("EmployeeLocations")
                        .HasForeignKey("EmployeeId")
                        .HasConstraintName("FK_EmployeeLocation_Employee")
                        .IsRequired();

                    b.HasOne("EFCodeFirstSamples.EFCoredbfirstModels.Location", "Location")
                        .WithMany("EmployeeLocations")
                        .HasForeignKey("LocationId")
                        .HasConstraintName("FK_EmployeeLocation_Location")
                        .IsRequired();
                });

            modelBuilder.Entity("EFCodeFirstSamples.EFCoredbfirstModels.EmployeeSkills", b =>
                {
                    b.HasOne("EFCodeFirstSamples.EFCoredbfirstModels.Employee", "Employee")
                        .WithMany("Skills")
                        .HasForeignKey("EmployeeId")
                        .HasConstraintName("FK_EmployeeSkill_Employee")
                        .IsRequired();

                    b.HasOne("EFCodeFirstSamples.EFCoredbfirstModels.Skill", "Skill")
                        .WithMany("EmployeeSkill")
                        .HasForeignKey("SkillId")
                        .HasConstraintName("FK_EmployeeSkill_Skill")
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
