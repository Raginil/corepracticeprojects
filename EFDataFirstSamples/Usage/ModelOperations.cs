﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCodeFirstSamples.EFCoredbfirstModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Logging;

namespace EFCodeFirstSamples.Usage
{
    public class ModelOperations
    {
        private readonly EFDataFirstPOCContext Db; 

        public ModelOperations(EFDataFirstPOCContext dbc)
        {
            Db = dbc;
        }

        public IList<Employee> GetAllEmployees()
        {
            //
            var result = Db.Employees.FromSqlRaw(@"Select * from Employee").ToList();

            return result;

        }

        public IList<Employee> SearchEmployeesByName(string EmployeeName)
        {            
            var employeename = new SqlParameter("@name", EmployeeName);         

            var result = Db.Employees.FromSqlRaw("execute sp_GetEmployeeByName @name", parameters: new[] { employeename });
            return result.ToList();
        }



        public Employee getEmployeeDetails(int EmployeeId)
        {
            var a = Db.Employees.Where(x => x.EmployeeId == EmployeeId).FirstOrDefault();
            return a;

        }

        public Employee getEmployeeSkills(int EmployeeId)
        {
            var a = Db.Employees.Where(x=>x.EmployeeId==EmployeeId).Include(x => x.Skills).FirstOrDefault();
            return a;
            
        }

        public IList<Employee> getAllEmployeeSkills()
        {
            var a = Db.Employees.Include(x => x.Skills).ThenInclude(skill=>skill.Skill).ToList();
            return a;
        }

        public EmployeeDetails getEmplyeeDetailsById(int id)
        {
            var result = Db.Employees.Where(x => x.EmployeeId == id)
                       .Select(a => new EmployeeDetails()
                       {
                           EmployeeId = a.EmployeeId,
                           EmployeeLocations = a.EmployeeLocations.ToList(),
                           EmployeeSkills = new List<EmployeeSkills>()


                       }).Single();

            return result;
        }

        
        public class EmployeeDetails
        {
            public int EmployeeId { get; set; }
            
            public List<EmployeeLocation> EmployeeLocations { get; set; }
            public List<EmployeeSkills> EmployeeSkills { get; set; }
        }

    }    
}