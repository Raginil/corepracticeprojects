﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("Employee")]
    public partial class Employee
    {
        internal object Skill;

        public Employee()
        {
            EmployeeLocations = new HashSet<EmployeeLocation>();
            Skills = new HashSet<EmployeeSkills>();
        }

        [Key]
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Salary { get; set; }

        [InverseProperty(nameof(EmployeeLocation.Employee))]
        public virtual ICollection<EmployeeLocation> EmployeeLocations { get; set; }

        [InverseProperty(nameof(EmployeeSkills.Employee))]
        public ICollection<EmployeeSkills> Skills { get;  set; }

        //[InverseProperty(nameof(EmployeeSkills.Skill))]
        //public virtual ICollection<EmployeeSkills> EmployeeSkill { get; set; }
    }
}
