﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("Skill")]
    public class Skill
    {
       

        public Skill()
        {
            EmployeeSkill = new HashSet<EmployeeSkills>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SkillId { get; set; }

        public string SkillName { get; set; }

        
        public string SkillType { get; set; }

        
        public ICollection<EmployeeSkills> EmployeeSkill { get; set; }

    }
}
