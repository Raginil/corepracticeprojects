﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("Department")]
    public partial class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        [InverseProperty("Departments")]
        public virtual Location Location { get; set; }
    }
}
