﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("School", Schema ="scl")] // Adding table with schema specification
    public class School
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public DateTime EstablishedDate { get; set; }
        public string Specification { get; set; }
        public string Location { get; set; }

        public SchoolType SchoolType { get; set; }
    }

    public enum SchoolType
    {
        PrmarySchool=1,
        UPSchool=2,
        HighSchool=3,
        College=4
    }
}
