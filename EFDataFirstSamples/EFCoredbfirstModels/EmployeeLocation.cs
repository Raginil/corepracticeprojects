﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("EmployeeLocation")]
    public partial class EmployeeLocation
    {
        [Key]
        public int EmployeeLocationId { get; set; }
        public int EmployeeId { get; set; }
        public int LocationId { get; set; }
        [Required]
        public string Locationype { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        [InverseProperty("EmployeeLocations")]
        public virtual Employee Employee { get; set; }
        [ForeignKey(nameof(LocationId))]
        [InverseProperty("EmployeeLocations")]
        public virtual Location Location { get; set; }
    }
}
