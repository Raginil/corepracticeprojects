﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("EmployeeSkills")]
    public class EmployeeSkills
    {
        [Key]
        [ForeignKey("EmployeeId")]
        public int EmployeeId { get; set; }

        [Key]
        [ForeignKey("SkillId")]
        public int SkillId { get; set; }

       
        public Employee Employee { get; set; }

        
        public Skill Skill { get;  set; }
    }
}
