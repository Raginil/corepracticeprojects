﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Logging;

#nullable disable

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    public partial class EFDataFirstPOCContext : DbContext
    {
        private readonly ILoggerFactory logger;
        public EFDataFirstPOCContext()
        {
        }

        public EFDataFirstPOCContext(DbContextOptions<EFDataFirstPOCContext> options,ILoggerFactory LoggerFactory)
            : base(options)
        {
            logger = LoggerFactory;
        }

        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeLocation> EmployeeLocations { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<EmployeeSkills> EmployeeSkills { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.

                optionsBuilder.UseLoggerFactory(logger).EnableSensitiveDataLogging(true)
                    .UseSqlServer("Data Source=DELL;Initial Catalog=EFDataFirstPOC;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);

                entity.Property(e => e.DepartmentName).IsUnicode(false);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Departments)
                    .HasForeignKey(x => x.LocationId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_Department_Location");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);

                entity.Property(e => e.Designation).IsUnicode(false);

                entity.Property(e => e.EmployeeName).IsUnicode(false);

                entity.Property(e => e.Salary).IsUnicode(false);
            });

            modelBuilder.Entity<EmployeeLocation>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);

                entity.Property(e => e.Locationype)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Home')");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EmployeeLocations)
                    .HasForeignKey(x => x.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeLocation_Employee");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.EmployeeLocations)
                    .HasForeignKey(x => x.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeLocation_Location");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);

                entity.Property(e => e.LocationName).IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasData(new Location { LocationId = 1, LocationName = "Hyderabad" },
                    new Location { LocationId = 2, LocationName = "Secunderabad" });

                
            });


            modelBuilder.Entity<Skill>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);               

                entity.Property(e => e.SkillName).IsUnicode(false);

                entity.Property(e => e.SkillType).IsUnicode(false);
            });

            modelBuilder.Entity<EmployeeSkills>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);
                entity.HasKey(a => new { a.EmployeeId, a.SkillId });
                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Skills)
                    .HasForeignKey(x => x.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeSkill_Employee");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.EmployeeSkill)
                    .HasForeignKey(x => x.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeSkill_Skill");
            });

            modelBuilder.Entity<School>(entity =>
            {
                entity.HasAnnotation("Relational:IsTableExcludedFromMigrations", false);

                entity.Property(e => e.SchoolName).IsUnicode(false);

                entity.Property(e => e.SchoolType).IsUnicode(false);

                entity.Property(e => e.EstablishedDate).IsUnicode(false);
                entity.Property(e => e.Specification).IsUnicode(false);
                entity.Property(e => e.Location).IsUnicode(false);
            });

            modelBuilder.Entity<School>(entity =>
            {
                entity.HasData(new School
                {
                    SchoolId=1,
                    SchoolName = "MppSchool",
                    SchoolType = SchoolType.PrmarySchool,
                    Location = "Hyderabad",
                    EstablishedDate =DateTime.Now,
                    Specification="mandatoryEducation"
                },

                new School
                {
                    SchoolId = 2,
                    SchoolName = "Abcschool",
                    SchoolType = SchoolType.HighSchool,
                    Location = "Hyderabad",
                    EstablishedDate = DateTime.Now,
                    Specification="StateSyllabus"
                });
            });

            modelBuilder.Entity<Skill>(e =>
            {
                e.HasData(new Skill
                {
                    SkillId=1,
                    SkillName="Dotnet",
                    SkillType="Software"
                },
                new Skill
                {
                    SkillId = 2,
                    SkillName = "sjfhbashd",
                    SkillType = "design"
                },
                new Skill
                {
                    SkillId = 3,
                    SkillName = "SqlServer",
                    SkillType = "Software"
                });

            });

            modelBuilder.Entity<Department>(e=>
            {
                e.HasData(new Department
                {
                    DepartmentId=1,
                    DepartmentName="IT"

                },
                new Department
                {
                    DepartmentId = 2,
                    DepartmentName = "Accounts"
                },
                new Department
                {
                    DepartmentId = 3,
                    DepartmentName = "Learning"
                });

            });

            modelBuilder.Entity<Employee>(e =>
            {
                e.HasData(new Employee
                {
                    EmployeeId=1,
                    EmployeeName="Employee1",
                    Designation="SE",
                    Salary="20000",
                    
                },
                new Employee
                {
                    EmployeeId = 2,
                    EmployeeName = "Employee2",
                    Designation = "SSE",
                    Salary = "30000"
                },
                new Employee
                {
                    EmployeeId = 3,
                    EmployeeName = "Employee3",
                    Designation = "SE",
                    Salary = "10000"
                });

            });


            modelBuilder.Entity<EmployeeLocation>(e =>
            {
                e.HasData(new EmployeeLocation
                {
                    EmployeeId=1,
                    EmployeeLocationId=1,
                    LocationId=1,
                    Locationype="Home"

                },
                new EmployeeLocation
                {
                    EmployeeId = 2,
                    EmployeeLocationId = 2,
                    LocationId = 2,
                    Locationype = "Office"

                },
                new EmployeeLocation
                {
                    EmployeeId = 1,
                    EmployeeLocationId = 3,
                    LocationId = 1,
                    Locationype = "Office"

                },
                new EmployeeLocation
                {
                    EmployeeId = 2,
                    EmployeeLocationId = 4,
                    LocationId = 2,
                    Locationype = "Home"

                });

            });


            modelBuilder.Entity<EmployeeSkills>(e =>
            {
                e.HasData(new EmployeeSkills
                {
                   SkillId=1,
                   EmployeeId=1

                },
                new EmployeeSkills
                {
                    SkillId = 1,
                    EmployeeId = 2
                },
                new EmployeeSkills
                {
                    SkillId = 2,
                    EmployeeId = 1
                },
                new EmployeeSkills
                {
                    SkillId = 2,
                    EmployeeId = 2
                });

            });



            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
