﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EFCodeFirstSamples.EFCoredbfirstModels
{
    [Table("Location")]
    public partial class Location
    {
        public Location()
        {
            Departments = new HashSet<Department>();
            EmployeeLocations = new HashSet<EmployeeLocation>();
        }

        [Key]
        public int LocationId { get; set; }
        public string LocationName { get; set; }

        [InverseProperty(nameof(Department.Location))]
        public virtual ICollection<Department> Departments { get; set; }
        [InverseProperty(nameof(EmployeeLocation.Location))]
        public virtual ICollection<EmployeeLocation> EmployeeLocations { get; set; }
    }
}
