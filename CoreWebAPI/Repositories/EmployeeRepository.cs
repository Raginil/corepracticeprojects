﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDBFirst;
using Microsoft.EntityFrameworkCore;

namespace CoreWebAPI.Repositories
{

    public class EmployeeRepository : IEmployeeRepository
    {

        private readonly EFCoreDBFContext db;

        public EmployeeRepository(EFCoreDBFContext _db)
        {
            db = _db;
        }
        public int AddEmployee(Employee objEmployee)
        {
            db.Employees.Add(objEmployee);
            if (objEmployee.EmployeeAddresses.Any())
            {
                db.EmployeeAddresses.AddRange(objEmployee.EmployeeAddresses);
            }
            db.SaveChanges();
            return objEmployee.Id;
        }

        public IList<Employee> GetAllEmployees()
        {
            return db.Employees.ToList();
        }

        public Employee GetById(int Id)
        {
            return db.Employees.Include(x => x.EmployeeAddresses).Where(a => a.Id.Equals(Id)).SingleOrDefault();
        }

        public int RemoveEmployee(int Id)
        {
            Employee objEmployee = db.Employees.FirstOrDefault(x => x.Id.Equals(Id));
            db.Employees.Remove(objEmployee);
            db.SaveChanges();
            return objEmployee.Id;
        }

        public int UpdateEmployee(Employee objEmployee)
        {            
            db.Employees.Update(objEmployee);
            if (objEmployee.EmployeeAddresses.Any())
            {
                foreach (var a in objEmployee.EmployeeAddresses)
                {
                    var address = db.EmployeeAddresses.FirstOrDefault(x => x.EmployeeId == objEmployee.Id && x.Id == a.Id);
                    if (address != null)
                    {
                        db.EmployeeAddresses.Update(address);
                    }
                    else
                    {
                        db.EmployeeAddresses.Add(a);
                    }
                }
            }
            db.SaveChanges();
            return objEmployee.Id;
        }
    }

}
