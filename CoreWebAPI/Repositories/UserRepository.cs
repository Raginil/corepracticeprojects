﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDBFirst;
using Microsoft.EntityFrameworkCore;

namespace CoreWebAPI.Repositories
{
    public class UserRepository:IUserRepository
    {
        private readonly EFCoreDBFContext db;

        public UserRepository(EFCoreDBFContext _db)
        {
            db = _db;
        }
        public int AddUser(User objUser)
        {
            db.Add(objUser);
            db.SaveChanges();
            return objUser.UserId;
        }

        public IList<User> GetAllUsers()
        {
            return db.Users.ToList();
        }

        public User GetById(int Id)
        {
            return db.Users.Where(a => a.UserId.Equals(Id)).SingleOrDefault();
        }

        public int RemoveUser(int Id)
        {
            User objUser = db.Users.FirstOrDefault(x => x.UserId.Equals(Id));
            db.Users.Remove(objUser);
            db.SaveChanges();
            return objUser.UserId;
        }

        public int UpdateUser(User objUser)
        {
            db.Users.Update(objUser);            
            db.SaveChanges();
            return objUser.UserId;
        }

        public User FindUser(string Username, string Password)
        {
           return db.Users.FirstOrDefault(x => x.UserName.Equals(Username) && x.Password.Equals(Password));
        }
    }
}
