﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDBFirst;

namespace CoreWebAPI.Repositories
{
    public interface IEmployeeRepository
    {
        public int AddEmployee(Employee objEmployee);
        public IList<Employee> GetAllEmployees();
        public Employee GetById(int Id);
        public int RemoveEmployee(int id);
        public int UpdateEmployee(Employee ObjEmployee);
    }
}
