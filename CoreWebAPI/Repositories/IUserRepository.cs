﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDBFirst;

namespace CoreWebAPI.Repositories
{
    public interface IUserRepository
    {
        public int AddUser(User objUser);
        public IList<User> GetAllUsers();
        public User GetById(int Id);

        public User FindUser(string Username, string Password);
        public int RemoveUser(int id);
        public int UpdateUser(User ObjUser);
    }
}
