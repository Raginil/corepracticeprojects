﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreWebAPI.Controllers
{
    [Route("api/v{version:apiVersion}/Values")]
    [ApiController]
    [ApiVersion("2.0")]
    public class SampleController : ControllerBase
    {
        
        /// <summary>
        /// GET all samples
        /// </summary>
        /// <returns>List of Sample objects</returns>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "SampleValue1", "SampleValue2" };
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        
               
        [HttpGet("{id}")]       
        public string Get(int id)
        {
            return "SampleValue"+id.ToString();
        }        
    }
}
