﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using EFCoreDBFirst;
using System.Security.Claims;
using CoreWebAPI.Repositories;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private IUserRepository _Repo { get; set; }             

        private IConfiguration Configuration;

        public UserController(IConfiguration config,
            IUserRepository Repository)
        {
            Configuration = config;
            _Repo = Repository;
           
        }

        
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
                       
           User objUser = _Repo.GetById(id);
               
            return Ok(objUser);
        }

        

        /// <summary>
        /// Post Request
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        // POST api/<UserController>

        [HttpPost]
        public ActionResult Post([FromBody] User obj)
        {
            
            if (ModelState.IsValid)
            {
                _Repo.AddUser(obj);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
            
        }

        /// <summary>
        /// Authenticates User
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Authenticate")]
        [AllowAnonymous]
        public ActionResult Authenticate([FromBody] User obj)
        {
            if (ModelState.IsValid)
            {
               User objUser = _Repo.FindUser(obj.UserName,obj.Password);
                var tokenString = GenerateJWT(objUser);
                return Ok(new { token = tokenString });
            }
            else
            {
                return BadRequest();
            }
            
            
        }
        private string GenerateJWT(User obj)
        {         

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));            
            permClaims.Add(new Claim("UserId", obj.UserId.ToString()));
              
            var token = new JwtSecurityToken(Configuration["Jwt:Issuer"],
                            Configuration["Jwt:Issuer"],
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
            
        }



    }
}
