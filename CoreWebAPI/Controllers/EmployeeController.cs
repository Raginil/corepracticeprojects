﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using EFCoreDBFirst;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using CoreWebAPI.Repositories;
using DinkToPdf.Contracts;
using DinkToPdf;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreWebAPI.Controllers
{
    
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        
        private IEmployeeRepository _Repo { get; set; }
        private IConverter _con { get; set; }
        

        public EmployeeController(IEmployeeRepository Repository, IConverter con)
        {
            _con = con;
            _Repo = Repository;
        }

        /// <summary>
        /// Get all Employees
        /// </summary>
        /// <returns></returns>
        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult<IEnumerable<Employee>> Get()
        {
            var Employees = _Repo.GetAllEmployees();
            if (Employees != null)
            {
                return Ok(Employees);
            }
            else
            {
                return NoContent();
            }
        }


        /// <summary>
        /// Get Employee By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/<EmployeeController>/5
        [HttpGet("{id}")] 
        public ActionResult<Employee> Get(int id)
        {
            var objEmployee = _Repo.GetById(id);

            if (objEmployee != null)
            {
                return Ok(objEmployee);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfile")]
        public ActionResult Getfile(int id)
        {
            var objEmployee = _Repo.GetById(id);

            string[] arr = new string[] { "cad", "sdfasd", "adfasd", "sdas" };

            if (objEmployee != null)
            {
                //var employees = 
                var sb = new StringBuilder();
                sb.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>This is the generated PDF report!!!</h1></div>
                                <table align='center'>
                                    <tr>
                                        <th>Name</th>
                                        <th>LastName</th>
                                        <th>Age</th>
                                        <th>Gender</th>
                                    </tr>");
                foreach (var emp in arr)
                {
                    sb.AppendFormat(@"<tr>
                                    <td>{0}</td>
                                    <td>{1}</td>
                                    <td>{2}</td>
                                    <td>{3}</td>
                                  </tr>", emp[0], emp[1], emp[2], emp[3]);
                }
                sb.Append(@"
                                </table>
                            </body>
                        </html>");
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    Out = @"D:\PDFCreator\Employee_Report.pdf"
                };
                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = sb.ToString(),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                    HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                    FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
                };
                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };
                _con.Convert(pdf);
                return Ok("Successfully created PDF document.");
            }
            else
            {
                return NoContent();
            }
            //return Ok("Successfully created PDF document.");
        }

        /// <summary>
        /// Creates New Employee
        /// </summary>
        /// <param name="objEmployee"></param>
        /// <returns></returns>
        // POST api/<EmployeeController>
        [HttpPost]
        public ActionResult Post([FromBody] Employee objEmployee)
        {
            if (ModelState.IsValid)
            {
                _Repo.AddEmployee(objEmployee);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
            
        }

        /// <summary>
        /// Updates Existing Employee
        /// </summary>
        /// <param name="id"></param>
        /// <param name="objEmployee"></param>
        /// <returns></returns>
        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Employee objEmployee)
        {
            if (ModelState.IsValid)
            {
                _Repo.UpdateEmployee(objEmployee);
                
                return Ok();
            }
            else
            {
                return BadRequest();
            }
           
        }

        /// <summary>
        /// Deleted Employee by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var objEmployee = _Repo.GetById(id);
               
            if (objEmployee != null)
            {                
                _Repo.RemoveEmployee(id);
                return Ok();
            }
            else
            {
                return NotFound();
            }
           
            
        }
    }
}
