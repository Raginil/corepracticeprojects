﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebAPI
{
    public class SwaggerVersionSetup : IConfigureOptions<SwaggerGenOptions>
    {
        readonly IApiVersionDescriptionProvider p;


        public SwaggerVersionSetup(IApiVersionDescriptionProvider provider) {
            this.p = provider;
        }
        public void Configure(SwaggerGenOptions options)
        {
            foreach (var description in p.ApiVersionDescriptions)
            {
                options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
            }
        }
        static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new OpenApiInfo()
            {
                Title = "SampleApiVersioning",
                Version = description.ApiVersion.ToString()
            };          

            return info;
        }
    }
}
