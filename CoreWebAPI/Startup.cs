﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CoreWebAPI.Models;
using CoreWebAPI.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using EFCoreDBFirst;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using CoreWebAPI.Controllers;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Xml.XPath;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using DinkToPdf.Contracts;
using DinkToPdf;

namespace CoreWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddDbContext<EFCoreDBFContext>(option =>
            {
                option.UseSqlServer(Configuration["connectionStrings:DBFConnectionString"]);
            });
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
                options.Conventions.Controller<ValuesController>().HasApiVersion(new ApiVersion(1, 0));
                options.Conventions.Controller<SampleController>().HasApiVersion(new ApiVersion(2, 0));
            });

            services.AddVersionedApiExplorer(
                options =>
                {                   
                    options.GroupNameFormat = "'v'VVV";                    
                    options.SubstituteApiVersionInUrl = true;
                });
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerVersionSetup>();
            services.AddSwaggerGen(
                options =>
                {                   
                    options.OperationFilter<SwaggerDefaultValues>();
                   
                    options.IncludeXmlComments(XmlCommentsFilePath);
                });
        

        

        
        string key = Configuration["Jwt:Key"];
            var issuer = Configuration["Jwt:Issuer"];

            services.AddAuthorization();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)

          .AddJwtBearer(options =>
          {
              options.TokenValidationParameters = new TokenValidationParameters
              {
                  ValidateIssuer = true,
                  ValidateAudience = true,
                  ValidateIssuerSigningKey = true,
                  ValidIssuer = issuer,
                  ValidAudience = issuer,
                  IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
              };
              options.SaveToken = true;

          });

            services.AddMvc();
        }

        

        public void Configure(IApplicationBuilder app, IApiVersionDescriptionProvider Provider, IWebHostEnvironment env)
        {
          

            app.UseHttpsRedirection();
            app.UseAuthentication();


            app.UseRouting();
            app.UseAuthorization();
            app.UseSwagger();
            
            app.UseSwaggerUI(
               options =>
               {                   
                    foreach (var description in Provider.ApiVersionDescriptions)
                   {
                       options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                   }
               });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors(x => x
                 .AllowAnyOrigin()
                 .AllowAnyMethod()
                 .AllowAnyHeader());



            app.UseExceptionHandler(
            options =>
            {
                options.Run(
                async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "text/html";
                    var ex = context.Features.Get<IExceptionHandlerFeature>();
                    if (ex != null)
                    {
                        var err = $"<h1>An error Has occured while processing your request. Please contact Admin</h1>";
                        await context.Response.WriteAsync(err).ConfigureAwait(false);
                    }
                });
            }
            );

        }

        static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }

    }
}
